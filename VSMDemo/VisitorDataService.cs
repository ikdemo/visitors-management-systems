﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace VSMDemo
{
    public class VisitorDataService
    {
        private DataTable _dtVisitors;
        public DataTable GetTempVisitors()
        {
            return getTempVisitors();
        }

        private DataTable getTempVisitors()
        {
            buildDataTable();

            return _dtVisitors;
        }

        private void buildDataTable()
        {
            if (_dtVisitors == null)
            {
                _dtVisitors = new DataTable();
                _dtVisitors.Columns.Add("Name");
                _dtVisitors.Columns.Add("GenderVal", typeof(int));
                _dtVisitors.Columns.Add("MobileExtn");
                _dtVisitors.Columns.Add("MobileNumber");
                _dtVisitors.Columns.Add("StaffId");
                _dtVisitors.Columns.Add("SignIn", typeof(DateTime));
                _dtVisitors.Columns.Add("SignOut", typeof(DateTime));
                _dtVisitors.Columns.Add("Duration", typeof(int));
                _dtVisitors.Columns.Add("StatusVal", typeof(int));
                _dtVisitors.Columns.Add("Gender");
                _dtVisitors.Columns.Add("Mobile");
                _dtVisitors.Columns.Add("Vendor");
                _dtVisitors.Columns.Add("Address");
                _dtVisitors.Columns.Add("MeetWith");
                _dtVisitors.Columns.Add("Reason");
                _dtVisitors.Columns.Add("VisitorCard");
                _dtVisitors.Columns.Add("TimeIn");
                _dtVisitors.Columns.Add("TimeOut");
                _dtVisitors.Columns.Add("TimeSpend");
                _dtVisitors.Columns.Add("Status");
            }
        }

        private void addDummyData()
        {
            DataRow dr = _dtVisitors.NewRow();
            dr["Name"] = "Murugan";
            dr["Gender"] = "Male";
            dr["Mobile"] = "+91 89848 51515";
            dr["Vendor"] = "Water Supply";
            dr["Address"] = "Chennai";
            dr["MeetWith"] = "Rajan - Admin";
            dr["Reason"] = "Water Supply";
            dr["VisitorCard"] = "FGD23453";
            dr["TimeIn"] = "11:00 AM";
            dr["TimeOut"] = "";
            dr["TimeSpend"] = "";
            dr["Status"] = "In";
            _dtVisitors.Rows.Add(dr);

            dr = _dtVisitors.NewRow();
            dr["Name"] = "Murugan";
            dr["Gender"] = "Male";
            dr["Mobile"] = "+91 89848 51515";
            dr["Vendor"] = "Interview";
            dr["Address"] = "Chennai";
            dr["MeetWith"] = "Gobal - HR";
            dr["Reason"] = "Interview";
            dr["VisitorCard"] = "FGD23444";
            dr["TimeIn"] = "09:00 AM";
            dr["TimeOut"] = "10:45 AM";
            dr["TimeSpend"] = "1 hr 45 min";
            dr["Status"] = "Out";
            _dtVisitors.Rows.Add(dr);
        }
    }
}