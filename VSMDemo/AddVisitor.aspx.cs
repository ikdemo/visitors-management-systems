﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VSMDemo
{
    public partial class AddVisitor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            populateData();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Dashboard.aspx");
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string name = txtFirstName.Text.Trim() + " " + txtLastName.Text.Trim();
            string gender = rblGender.SelectedItem.Text;
            string mobile = txtMobileNumber.Text.Trim();
            string vendor = txtVendor.Text.Trim();
            string address = txtAddress.Text.Trim();
            string meet = ddlStaffs.SelectedItem.Text;
            string reason = txtReason.Text.Trim();
            string visitCard = ddlVisitorCard.SelectedItem.Text;
            addNewVisitor(name, gender, mobile, vendor, address, meet, reason, visitCard);

            Response.Redirect("Dashboard.aspx");
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddVisitor.aspx");
        }

        private void addNewVisitor(string name, string gender, string mobile, string vendor, string address, string meetWith, string reason,
            string visitorCard)
        {
            DataTable dtData = null;
            dtData = Session["Visitor"] as DataTable;
            if (dtData != null)
            {
                DataRow dr = dtData.NewRow();

                dr["VisitorId"] = getNewVisitorId(dtData);
                dr["Name"] = name;
                dr["Gender"] = gender;
                dr["Mobile"] = mobile;
                dr["Vendor"] = vendor;
                dr["Address"] = address;
                dr["MeetWith"] = meetWith;
                dr["Reason"] = reason;
                dr["VisitorCard"] = visitorCard;
                dr["Signin"] = DateTime.Now;
                dr["TimeIn"] = DateTime.Parse(dr["Signin"].ToString()).ToString("hh:mm tt");
                dr["TimeOut"] = "";
                dr["TimeSpend"] = "";
                dr["Status"] = "In";

                dtData.Rows.Add(dr);

                Session["Visitor"] = dtData;
            }
        }

        private string getNewVisitorId(DataTable dtData)
        {
            string visitorId = string.Empty;

            if (dtData != null && dtData.Rows.Count > 0)
            {
                visitorId = (dtData.Rows.Count + 1).ToString();
            }

            return visitorId;
        }

        private DataTable populateData()
        {
            DataTable dtData = null;
            dtData = Session["Visitor"] as DataTable;
            if (dtData == null)
            {
                dtData = new DataTable();
                dtData.Columns.Add("VisitorId");
                dtData.Columns.Add("Name");
                dtData.Columns.Add("Gender");
                dtData.Columns.Add("Mobile");
                dtData.Columns.Add("Vendor");
                dtData.Columns.Add("Address");
                dtData.Columns.Add("MeetWith");
                dtData.Columns.Add("Reason");
                dtData.Columns.Add("VisitorCard");
                dtData.Columns.Add("TimeIn");
                dtData.Columns.Add("TimeOut");
                dtData.Columns.Add("Signin", typeof(DateTime));
                dtData.Columns.Add("SignOut", typeof(DateTime));
                dtData.Columns.Add("TimeSpend");
                dtData.Columns.Add("Status");

                DataRow dr = dtData.NewRow();
                dr["VisitorId"] = "2";
                dr["Name"] = "Murugan";
                dr["Gender"] = "Male";
                dr["Mobile"] = "+91 89848 51515";
                dr["Vendor"] = "Water Supply";
                dr["Address"] = "Chennai";
                dr["MeetWith"] = "Rajan - Admin";
                dr["Reason"] = "Water Supply";
                dr["VisitorCard"] = "FGD23453";
                dr["Signin"] = DateTime.Now.AddHours(-2);
                //dr["SignOut"] = "";
                dr["TimeIn"] = DateTime.Parse(dr["Signin"].ToString()).ToString("hh:mm tt");
                dr["TimeOut"] = "";
                dr["TimeSpend"] = "";
                dr["Status"] = "In";
                dtData.Rows.Add(dr);

                dr = dtData.NewRow();
                dr["VisitorId"] = "1";
                dr["Name"] = "Lalitha";
                dr["Gender"] = "Female";
                dr["Mobile"] = "+91 79848 34222";
                dr["Vendor"] = "Interview";
                dr["Address"] = "Chennai";
                dr["MeetWith"] = "Gobal - HR";
                dr["Reason"] = "Interview";
                dr["VisitorCard"] = "FGD23444";
                dr["Signin"] = DateTime.Parse("06/01/2021 06:30:00");
                dr["SignOut"] = DateTime.Parse("06/01/2021 07:35:00");
                dr["TimeIn"] = DateTime.Parse(dr["Signin"].ToString()).ToString("hh:mm tt");
                dr["TimeOut"] = DateTime.Parse(dr["SignOut"].ToString()).ToString("hh:mm tt");
                dr["TimeSpend"] = "1 hr 5 min";
                dr["Status"] = "Out";
                dtData.Rows.Add(dr);

                Session["Visitor"] = dtData;
            }

            return dtData;
        }
    }
}