﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="VSMDemo.Dashboard" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head runat="server">
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>
    <div class="jumbotron text-center">
        <h1>Visitors Management Systems</h1>
        <p>This is ASP.Net learning project</p>
    </div>
    <div class="container-fluid">
        <form id="form1" runat="server">
            <div>
                <h3>Today Visitors Log</h3>
            </div>
            <div class="text-right">
                <asp:Button ID="btnAddVisitor" runat="server" Text="Add Visitor" OnClick="btnAddVisitor_Click" CssClass="btn btn-link"/>
                <asp:Button ID="btnViewHistory" runat="server" Text="View Past History" CssClass="btn btn-link"/>
            </div>
            <div style="margin-top: 20px; overflow: auto;">
                <asp:GridView ID="gvVisitors" runat="server" AutoGenerateColumns="false" OnRowCommand="gvVisitors_RowCommand" CssClass="table table-striped" HeaderStyle-CssClass="thead-dark">
                    <Columns>
                        <asp:TemplateField HeaderText="#">
                            <ItemTemplate>
                                <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Name" DataField="Name" />
                        <asp:BoundField HeaderText="Gender" DataField="Gender" />
                        <asp:BoundField HeaderText="Mobile" DataField="Mobile" />
                        <asp:BoundField HeaderText="Vendor" DataField="Vendor" />
                        <asp:BoundField HeaderText="Address" DataField="Address" />
                        <asp:BoundField HeaderText="Meeting With" DataField="MeetWith" />
                        <asp:BoundField HeaderText="Reason" DataField="Reason" />
                        <asp:BoundField HeaderText="Visitor Card #" DataField="VisitorCard" />
                        <asp:BoundField HeaderText="Time In" DataField="TimeIn" />
                        <asp:BoundField HeaderText="Time Out" DataField="TimeOut" />
                        <asp:BoundField HeaderText="Time Spend" DataField="TimeSpend" />
                        <asp:BoundField HeaderText="Status" DataField="Status" />
                        <asp:TemplateField HeaderText="Action">
                            <ItemTemplate>
                                <asp:Button ID="btnSignOut" runat="server" Text="Sign Out" CssClass="btn btn-primary" Visible='<%# Eval("Status").ToString() == "In" %>' CommandArgument='<%# Eval("VisitorId") %>' CommandName="SignOut" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </form>
    </div>
</body>
</html>
