﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VSMDemo
{
    public partial class Dashboard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                gvVisitors.DataSource = populateData();
                gvVisitors.DataBind();
            }
        }

        protected void gvVisitors_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "SignOut":
                    string visitorId = e.CommandArgument.ToString();
                    if (visitorId != string.Empty)
                    {
                        signOutVisitor(visitorId);

                        Response.Redirect("Dashboard.aspx");
                    }
                    break;
            }
        }

        protected void btnAddVisitor_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddVisitor.aspx");
        }

        private DataTable populateData()
        {
            DataTable dtData = null;
            dtData = Session["Visitor"] as DataTable;
            if (dtData == null)
            {
                dtData = new DataTable();
                dtData.Columns.Add("VisitorId");
                dtData.Columns.Add("Name");
                dtData.Columns.Add("Gender");
                dtData.Columns.Add("Mobile");
                dtData.Columns.Add("Vendor");
                dtData.Columns.Add("Address");
                dtData.Columns.Add("MeetWith");
                dtData.Columns.Add("Reason");
                dtData.Columns.Add("VisitorCard");
                dtData.Columns.Add("TimeIn");
                dtData.Columns.Add("TimeOut");
                dtData.Columns.Add("Signin", typeof(DateTime));
                dtData.Columns.Add("SignOut", typeof(DateTime));
                dtData.Columns.Add("TimeSpend");
                dtData.Columns.Add("Status");

                DataRow dr = dtData.NewRow();
                dr["VisitorId"] = "2";
                dr["Name"] = "Murugan";
                dr["Gender"] = "Male";
                dr["Mobile"] = "+91 89848 51515";
                dr["Vendor"] = "Water Supply";
                dr["Address"] = "Chennai";
                dr["MeetWith"] = "Rajan - Admin";
                dr["Reason"] = "Water Supply";
                dr["VisitorCard"] = "FGD23453";
                dr["Signin"] = DateTime.Now.AddHours(-2);
                //dr["SignOut"] = "";
                dr["TimeIn"] = DateTime.Parse(dr["Signin"].ToString()).ToString("hh:mm tt");
                dr["TimeOut"] = "";
                dr["TimeSpend"] = "";
                dr["Status"] = "In";
                dtData.Rows.Add(dr);

                dr = dtData.NewRow();
                dr["VisitorId"] = "1";
                dr["Name"] = "Lalitha";
                dr["Gender"] = "Female";
                dr["Mobile"] = "+91 79848 34222";
                dr["Vendor"] = "Interview";
                dr["Address"] = "Chennai";
                dr["MeetWith"] = "Gobal - HR";
                dr["Reason"] = "Interview";
                dr["VisitorCard"] = "FGD23444";
                dr["Signin"] = DateTime.Now.AddHours(-3);
                dr["SignOut"] = DateTime.Now.AddHours(-2).AddMinutes(5);
                dr["TimeIn"] = DateTime.Parse(dr["Signin"].ToString()).ToString("hh:mm tt");
                dr["TimeOut"] = DateTime.Parse(dr["SignOut"].ToString()).ToString("hh:mm tt");
                dr["TimeSpend"] = "1 hr 5 min";
                dr["Status"] = "Out";
                dtData.Rows.Add(dr);

                dr = dtData.NewRow();
                dr["VisitorId"] = "3";
                dr["Name"] = "Madhan";
                dr["Gender"] = "Kumar";
                dr["Mobile"] = "+91 84544 35333";
                dr["Vendor"] = "Interview";
                dr["Address"] = "Chennai";
                dr["MeetWith"] = "John - HR";
                dr["Reason"] = "Interview";
                dr["VisitorCard"] = "VSDH23322";
                dr["Signin"] = DateTime.Parse("06/01/2021 06:30:00");
                dr["TimeIn"] = DateTime.Parse(dr["Signin"].ToString()).ToString("hh:mm tt");
                dr["Status"] = "In";
                dtData.Rows.Add(dr);

                Session["Visitor"] = dtData;
            }

            dtData = dtData.Select("", "Status, VisitorId").CopyToDataTable();

            return dtData;
        }

        private void signOutVisitor(string visitorId)
        {
            DataTable dtData = Session["Visitor"] as DataTable;
            if (dtData != null)
            {
                DataRow[] drRows = dtData.Select("VisitorId = '" + visitorId + "'");
                if (drRows != null && drRows.Length > 0)
                {
                    drRows[0]["Status"] = "Out";
                    drRows[0]["SignOut"] = DateTime.Now;
                    drRows[0]["TimeOut"] = DateTime.Parse(drRows[0]["SignOut"].ToString()).ToString("hh:mm tt");

                    DateTime timeIn = DateTime.Parse(drRows[0]["SignIn"].ToString());
                    DateTime timeOut = DateTime.Parse(drRows[0]["SignOut"].ToString());

                    drRows[0]["TimeSpend"] = getDuration(timeIn, timeOut);
                }

                Session["Visitor"] = dtData;
            }
        }

        private string getDuration(DateTime timeIn, DateTime timeOut)
        {
            string duration = string.Empty;
            var dur = timeOut - timeIn;

            if (dur.Hours > 0)
            {
                duration = dur.Hours + " hrs";
            }

            if (dur.Minutes > 0 && (dur.Minutes % 60) > 0)
            {


                duration += " " + (dur.Minutes % 60) + " min";
            }

            return duration;
        }
    }
}