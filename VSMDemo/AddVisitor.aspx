﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddVisitor.aspx.cs" Inherits="VSMDemo.AddVisitor" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head runat="server">
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <style type="text/css">
        .radiostyle {
            height: auto;
            border: 0px !important;
        }

            .radiostyle label {
                margin-left: 3px !important;
                margin-right: 10px !important;
            }
    </style>
</head>
<body>
    <div class="jumbotron text-center">
        <h1>Visitors Management Systems</h1>
        <p>This is ASP.Net learning project</p>
    </div>
    <div class="container">
        <form id="form1" runat="server">
            <div>
                <h3>Add Visitor</h3>
            </div>
            <div class="text-right mt-5">
                <span class="text-danger font-weight-bold">*</span>
                <asp:Label ID="lblMessage" runat="server" Text="Please fill all the madatory fields" CssClass="text-muted" />
            </div>
            <div>
                <div class="form-group">
                    <div class="font-weight-bold">
                        <span class="text-danger">*</span>
                        <asp:Label ID="lblFirstName" runat="server" Text="First Name" />
                    </div>
                    <div>
                        <asp:TextBox ID="txtFirstName" runat="server" CssClass="form-control mt-2" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="font-weight-bold">
                        <span class="text-danger">*</span>
                        <asp:Label ID="lblLastName" runat="server" Text="Last Name" />
                    </div>
                    <div>
                        <asp:TextBox ID="txtLastName" runat="server" CssClass="form-control mt-2" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="font-weight-bold">
                        <span class="text-danger">*</span>
                        <asp:Label ID="lblGender" runat="server" Text="Gender" />

                    </div>
                    <div>
                        <asp:RadioButtonList ID="rblGender" runat="server" RepeatDirection="Horizontal" CssClass="form-control radiostyle">
                            <asp:ListItem Text="Male" Value="1" />
                            <asp:ListItem Text="Female" Value="2" />
                            <asp:ListItem Text="Transgender" Value="3" />
                        </asp:RadioButtonList>
                    </div>
                </div>
                <div class="form-group">
                    <div class="font-weight-bold">
                        <span class="text-danger">*</span>
                        <asp:Label ID="lblMobile" runat="server" Text="Mobile #" />
                    </div>
                    <div>
                        <asp:TextBox ID="txtMobileNumber" runat="server" CssClass="form-control mt-2" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="font-weight-bold">
                        <asp:Label ID="lblVendor" runat="server" Text="Vendor" />
                    </div>
                    <div>
                        <asp:TextBox ID="txtVendor" runat="server" CssClass="form-control mt-2" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="font-weight-bold">
                        <asp:Label ID="lblAddress" runat="server" Text="Address" />

                    </div>
                    <div>
                        <asp:TextBox ID="txtAddress" runat="server" CssClass="form-control mt-2" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="font-weight-bold">
                        <span class="text-danger">*</span>
                        <asp:Label ID="lblMeetWith" runat="server" Text="Meeting With" />
                    </div>
                    <div>
                        <asp:DropDownList ID="ddlStaffs" runat="server" CssClass="form-control mt-2">
                            <asp:ListItem Text="Select staff here" Value="0" Selected="True" />
                            <asp:ListItem Text="Rajan - Admin" Value="1" />
                            <asp:ListItem Text="Gobal - IT" Value="2" />
                            <asp:ListItem Text="John - HR" Value="3" />
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form-group">
                    <div class="font-weight-bold">
                        <span class="text-danger">*</span>
                        <asp:Label ID="lblReason" runat="server" Text="Reason" />
                    </div>
                    <div>
                        <asp:TextBox ID="txtReason" runat="server" CssClass="form-control mt-2" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="font-weight-bold">
                        <span class="text-danger">*</span>
                        <asp:Label ID="lblVisitCard" runat="server" Text="Visitor Card #" />
                    </div>
                    <div>
                        <asp:DropDownList ID="ddlVisitorCard" runat="server" CssClass="form-control mt-2">
                            <asp:ListItem Text="Select Card here" Value="0" Selected="True" />
                            <asp:ListItem Text="VSMFF12134" Value="1" />
                            <asp:ListItem Text="VSDH23322" Value="2" />
                            <asp:ListItem Text="DEWWSR3546" Value="3" />
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form-group">
                    <div class="font-weight-bold">
                        <asp:Label ID="lblTimeIn" runat="server" Text="Time In" />
                    </div>
                    <div>
                        <asp:Label ID="lblCurrentTime" runat="server" />
                    </div>
                </div>
                <div class="form-group text-center">
                    <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn btn-primary" />
                    <asp:Button ID="btnReset" runat="server" Text="Reset" OnClick="btnReset_Click" CssClass="btn btn-outline-dark ml-2" />
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" CssClass="btn btn-outline-danger ml-2" />
                </div>
            </div>
        </form>
    </div>
</body>
</html>
